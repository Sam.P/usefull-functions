Function Restart-Process {
    
[CmdletBinding()]
    Param()

    DynamicParam {

        $ParameterName = 'Name' 
        $RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
        $ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttribute.Mandatory = $false
        $ParameterAttribute.Position = 1
        $AttributeCollection.Add($ParameterAttribute) 
        $arrSet = Get-Process | Select-Object -ExpandProperty Name
        $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($arrSet)
        $AttributeCollection.Add($ValidateSetAttribute)

        $ParameterName1 = 'Id'
        $RuntimeParameterDictionary1 = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $AttributeCollection1 = New-Object System.Collections.ObjectModel.Collection[System.Attribute] 
        $ParameterAttribute1 = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttribute1.Mandatory = $false
        $ParameterAttribute1.Position = 2
        $AttributeCollection1.Add($ParameterAttribute1)
        $arrSet1 = Get-Process | Select-Object -ExcludeProperty Id
        $ValidateSetAttribute1 = New-Object System.Management.Automation.ValidateSetAttribute($arrSet1)
        $AttributeCollection1.Add($ValidateSetAttribute1)

        $RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter($ParameterName, [string], $AttributeCollection)
        $RuntimeParameter1 = New-Object System.Management.Automation.RuntimeDefinedParameter($ParameterName1, [int], $AttributeCollection1)

        $RuntimeParameterDictionary.Add($ParameterName, $RuntimeParameter)
        $RuntimeParameterDictionary.Add($ParameterName1, $RuntimeParameter1)


        return $RuntimeParameterDictionary
    }
    
    begin {
        # Bind the parameter to a friendly variable
        $Name = $PsBoundParameters[$ParameterName]
        $Id = $PsBoundParameters[$ParameterName1]
    }
    
    process {

        # // if Restart-process et c'est tout
        if( (-not $Name) -and (-not $Id) ) {
            return "Impossible de lancer la fonction Restart-Process ! Celle-ci a besoins du paramètre « -Name » ou « -Id »."
        }

        if($Name) {
            $process = (Get-Process -Name $Name).ProcessName
            Stop-Process -Name $process -PassThru -Force | Out-Null
            Start-Sleep 1

            if (-not (Get-Process -Name $Name -ErrorAction Ignore)) {
                try { 
                    Start-Process $Name -PassThru | Out-Null
                    return "Le processus « $Name » s'est bien redémarré !"
                }
                catch { 
                    return "Le processus « $Name » s'est mal redémarré, veuillez le redémarrer à la main si possible..."
                }
            }
            elseif (Get-Process -Name $Name -ErrorAction Ignore) {
                Stop-Process -Name $process -PassThru -Force | Out-Null
                Start-Sleep 1
                if (Get-Process -Name $Name -ErrorAction Ignore) {
                    return "Le processus « $Name » s'est mal arrêté."
                }
            }
        }
        if($Id) {
            $process = (Get-Process -Id $Id).ProcessName
            Stop-Process -Id $Id -PassThru -Force | Out-Null
            Start-Sleep 1
            if (-not (Get-Process -Id $Id -ErrorAction Ignore)) {
                try { 
                    Start-Process $process -PassThru | Out-Null
                    return "Le processus d'Id « $Id » s'est bien redémarré !"
                }
                catch { 
                    return "Le processus d'Id « $Id » s'est mal redémarré, veuillez le redémarrer à la main si possible..."
                }

            } elseif (Get-Process -Id $Id -ErrorAction Ignore) {
                try { 
                    Stop-Process -Id $Id -PassThru -Force | Out-Null
                    Start-Sleep 1
                    if (-not (Get-Process -Id $Id -ErrorAction Ignore)) {
                        try { 
                            Start-Process $process -PassThru | Out-Null
                            return "Le processus d'Id « $Id » s'est bien redémarré !"
                        }
                        catch { return "Le processus d'Id « $Id » s'est mal arrêté." }
                    }
                }
                catch {
                    return "Le processus d'Id « $Id » s'est mal arrêté."
                }
            }
        } else {
            return "Le processus est déjà étéint."
          }
    }
}

function Find-Path { 
    [CmdletBinding()]
        Param(
            [Parameter(Mandatory=$false, Position=0)]
            [string]$Name,
            [Parameter(Mandatory=$false, Position=1)]
            [string]$Extension,
            [Parameter(Mandatory=$false, Position=2)]
            [switch]$Help
        )
    
    if( (-not $Name) -and (-not $Extension) -and (-not $Help) ) { Write-Error "You need to use parameters (-Name '' ) OR (-Name '' -Extension '' ) OR ( -Help ) " -ErrorAction Stop }
    if( (-not $help) -and (-not $Name) -and ($Extension) ) { Write-Error "You need to use parameters (-Name '' ) OR (-Name '' -Extension '' ) OR ( -Help ) " -ErrorAction Stop }
    if($Help) {
        if(($Name) -or ($Extension)) {
            Write-Error "Si -Help, pas -Name ou -Extension" -ErrorAction Stop
        } else { Write-Output "This Function is used to Find paths of apps or files" ; Get-Help "Find-Path" }
    } else {

        if($Extension.Contains(".")) { Write-Error "Don't -extension with : '.' `nRight way like : -extension 'exe' " -ErrorAction Stop }
        if(-not $Extension) { $Extension = "exe" }
            
        $FullName = "${Name}.${Extension}"

        if(Test-Path "$env:USERPROFILE\PathsLOG.txt") { Remove-Item "$env:USERPROFILE\PathsLOG.txt" -Force -ErrorAction Stop | Out-Null -ErrorAction Ignore }
        $Volumes = (Get-Volume -ErrorAction Continue).DriveLetter
        New-Item "$env:USERPROFILE\PathsLOG.txt" -Force -ErrorAction Stop | Out-Null -ErrorAction Ignore
            
        # 1)
        $getTest = (Get-ChildItem -Force -Path "$env:ProgramFiles" -Include "*${FullName}" -File -Recurse -ErrorAction Ignore).DirectoryName 
        if($getTest) { $getTest | Out-File "$env:USERPROFILE\PathsLOG.txt" -Force -Append -ErrorAction Ignore }
        $content = Get-Content "$env:USERPROFILE\PathsLOG.txt" -Force -ErrorAction Ignore

        if(-not $content) {
            # 2)
            $getTest = (Get-ChildItem -Force -Path "${env:ProgramFiles(x86)}" -Include "*${FullName}" -File -Recurse -ErrorAction Ignore).DirectoryName
            if($getTest) { $getTest | Out-File "$env:USERPROFILE\PathsLOG.txt" -Force -Append -ErrorAction Ignore }
            $content = Get-Content "$env:USERPROFILE\PathsLOG.txt" -Force -ErrorAction Ignore
            "cc"
            } 


        if(-not $content) {
            # 3)    
            $getTest = (Get-ChildItem -Force -Path "$env:USERPROFILE" -Include "*${FullName}" -File -Recurse -ErrorAction Ignore).DirectoryName 
            if($getTest) { $getTest | Out-File "$env:USERPROFILE\PathsLOG.txt" -Force -Append -ErrorAction Ignore }
            $content = Get-Content "$env:USERPROFILE\PathsLOG.txt" -Force -ErrorAction Ignore
        }
        if (-not $content) { 
            # 4)
            foreach ($letter in $Volumes) {
                $getTest = (Get-ChildItem -Force -Path "${letter}:\" -Include "*${FullName}" -File -Recurse -ErrorAction Ignore).DirectoryName 
                if($getTest) { (Get-ChildItem -Force -Path "${letter}:\" -Include "*${FullName}" -File -Recurse -ErrorAction Ignore).DirectoryName | Out-File "$env:USERPROFILE\PathsLOG.txt" -Force -Append -ErrorAction Ignore }
            }
        }

        $content = Get-Content "$env:USERPROFILE\PathsLOG.txt" -Force -ErrorAction Ignore
        if ($content) {                 
            foreach ($Lines in $content) { Write-Output "$Lines" } 
        } else { Write-Error "Pas de path trouvé pour $FullName dans les disques : $Volumes" }    
    }
}
